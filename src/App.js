import React, {useState} from 'react';
import './App.css';
import Slider from './components/Slider'
import SidebarItem from './components/SidebarItem'

const DEFAULT_OPTIONS = [
  {
    name: 'Brightness',
    property: 'brightness',
    value: 100,
    range: {
      min: 0,
      max: 200,
    },
    unit: '%'
  },
  {
    name: 'Contrast',
    property: 'contrast',
    value: 100,
    range: {
      min: 0,
      max: 200,
    },
    unit: '%'
  },

  {
    name: 'Saturation',
    property: 'saturation',
    value: 100,
    range: {
      min: 0,
      max: 200,
    },
    unit: '%'
  },

  {
    name: 'Grayscale',
    property: 'grayscale',
    value: 0,
    range: {
      min: 0,
      max: 100,
    },
    unit: '%'
  },

  {
    name: 'Sepia',
    property: 'sepia',
    value: 0,
    range: {
      min: 0,
      max: 100,
    },
    unit: '%'
  },

  {
    name: 'Hue Rotate',
    property: 'hue-rotate',
    value: 0,
    range: {
      min: 0,
      max: 360,
    },
    unit: 'deg'
  },

  {
    name: 'Blur',
    property: 'blur',
    value: 0,
    range: {
      min: 0,
      max: 20,
    },
    unit: 'px'
  },
]

function App() {
  const[options, setOptions] = useState(DEFAULT_OPTIONS) 
  // La primera es el nombre de la variable de estado que se pretende cambiar. La segunda el método que se usará y como parámetro a useState, de dónde se toma la info. 
  return (
    <div className="container">
      <div className="main-image" />
      <div className="sidebar">
        {options.map((option,index) => {
          return  (<SidebarItem 
          key={index}
          name={option.name}
          />
          )
        })}
        
        
      </div>
      <Slider />
    </div>
  );
}

export default App;
